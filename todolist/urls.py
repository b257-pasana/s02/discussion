from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name="index"),
    # localhost:8000/todolist/1
    path('<int:todoitem_id>/', views.todoitem, name="viewtodoitem"),
    # localhost:8000/todolist/register
    path('register/', views.register, name="register"),
    # localhost:8000/todolist/change-password
    path('change-password/', views.change_password, name="change_password"),
    # localhost:8000/todolist/login
    path('login/', views.login_view, name="login"),
    # localhost:8000/todolist/logout
    path('logout/', views.logout_view, name="logout")
]